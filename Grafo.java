﻿package grafos;
import java.io.*;
import java.util.*;

public class Grafo {
    //Code by: Rodrigo Alcayaga, Francisco Thenoux
    
    @SuppressWarnings("FieldMayBeFinal")
    private java.util.List<Nodo> nodos;
    
    public void main(String[] args) throws Exception {
        BufferedReader bf = new BufferedReader (new InputStreamReader (System.in));
        System.out.print("Qué desea hacer?"
                + "\n 1) Agregar nodo."
                + "\n 2) Eliminar nodo."
                + "\n 3) Listar Adyacentes."
                + "\n 4) Es Adyacente."
                + "\n 5) Renombrar Nodo."
                + "\n 6) Agregar Adyacente."
                + "\n 7) Modificar Adyacentes."
                + "\n 8) Eliminar Adyacente."
                + "\n 9) Actualizar Peso."
                + "\n Ingrese Acción: ");
                
                int x=Integer.parseInt(bf.readLine());
        
                switch(x){
                    case 1: 
                        System.out.println("Ingrese nombre: ");
                        String nomNodo = bf.readLine();
                        agregarNodo(nomNodo);
                        break;
                    case 2: 
                        System.out.println("Ingrese posicion del nodo a eliminar: ");
                        // se le resta 1 al valor para que coincida con el vector que inicia en 0
                        int delNodo = Integer.parseInt(bf.readLine()) - 1;
                        eliminarNodo(nodos.get(delNodo));
                        break;
                    case 3: 
                        System.out.println("Ingrese posicion del nodo: ");
                        int listAdy = Integer.parseInt(bf.readLine()) - 1;
                        nodos.get(listAdy).listarAdyacentes();
                        break;
                    case 4: 
                        System.out.println("Ingrese posicion del nodo: ");
                        int esAdy = Integer.parseInt(bf.readLine()) - 1;
                        if(nodos.get(esAdy).esAdyacente() == true){
                            System.out.println("\nEs adyacente.");
                        }else{
                            System.out.println("\nNo es adyacente.");                            
                        }
                        break;
                    case 5: 
                        System.out.println("Ingrese posicion del nodo: ");
                        // almaceno posicion del elemento en la lista
                        int renomNod = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese nuevo nombre: ");
                        String nuevoNombre = bf.readLine();
                        /* activa el metodo renombrar del elemento seleccionado
                        con el nuevo nombre como parametro */
                        nodos.get(renomNod).renombrar(nuevoNombre);
                        break;
                    case 6: 
                        System.out.println("Ingrese posicion del nodo de origen: ");
                        // este nodo es el mismo que hará el llamado del procedimiento
                        int nodoOrigen = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese posicion del nodo de destino: ");
                        int nodoDestino = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese peso del arco: ");
                        int pesoArco = Integer.parseInt(bf.readLine());
                        /* el nodo de origen hace el llamado y se manda a el
                        mismo como parametro, además del nodo destino y el peso */
                        nodos.get(nodoOrigen).agregarAdyacente(nodos.get(nodoOrigen), nodos.get(nodoDestino), pesoArco);
                        break;
                    case 7:
                        System.out.println("Ingrese posicion del nodo de origen: ");
                        int modOrigen = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese posicion del Arco: ");
                        int modArco = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese posicion del nuevo nodo de destino: ");
                        int modDestino = Integer.parseInt(bf.readLine()) - 1;
                        /* se elige el nodo, se elige la arista de ese nodo
                        y se envia el nuevo nodo de detino como parametro */
                        nodos.get(modOrigen).modificarAdyacente(nodos.get(modDestino), modArco);
                        break;
                    case 8: 
                        System.out.println("Ingrese posicion del nodo : ");
                        int posNodo = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese posicion del Arco: ");
                        //int posicion = arista.IndexOf(arquin); --> busca la posicion del arco
                        int posArco = Integer.parseInt(bf.readLine()) - 1;
                        /* el nodo seleccionado hace el llamado y manda el
                        numero de la posicion del arco como parametro */
                        nodos.get(posNodo).eliminarAdyacente(posArco);
                        break;
                    case 9: 
                        System.out.println("Ingrese posicion del nodo : ");
                        int eligeNodo = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese posicion del Arco: ");
                        int eligeArco = Integer.parseInt(bf.readLine()) - 1;
                        System.out.println("Ingrese nuevo peso: ");
                        int nuevoPeso = Integer.parseInt(bf.readLine());
                        /* se elige el nodo, se elige la arista de ese nodo
                        y se envia el nuevo peso del arco como parametro */
                        nodos.get(eligeNodo).arista.get(eligeArco).actualizarPeso(nuevoPeso);
                        break;
                    default: 
                        System.out.println("El número que eligió no es correcto.");
                        break;
                    }

    }

    
    
    //::::::::::::: CONSTRUCTORES ::::::::::::::
    
    public Grafo() {
        this.nodos = new LinkedList<Nodo>();
    }
    
    
    
    //:::::::::: METODOS Y FUNCIONES :::::::::::
    
    public void agregarNodo(String nom){
        Nodo nodin = new Nodo(nom);
        // agrega un nodo a la lista
        nodos.add(nodin);                       
    }

    public void eliminarNodo(Nodo nodin) {
        // almacena la posicion del elemento del vector donde se encuentra el nodo
        int posicion = nodos.indexOf(nodin);
        // elimina el nodo en la posicion seleccionada
        nodos.remove(posicion);
    }
    
}