package grafos;
import java.util.*;

public class Nodo {
    //Code by: Rodrigo Alcayaga, Francisco Thenoux
    String nombre = "";
    LinkedList<Arco> arista=new LinkedList<> ();
    
    
    //::::::::::: CONSTRUCTORES :::::::::::::

    Nodo(String nom) {
        this.nombre = nom;
        this.arista = new LinkedList<Arco>();
    }
    
    
    //:::::::: METODOS Y FUNCIONES :::::::::
    
    public boolean esAdyacente() {
        // almacena en "vacio" true si la lista esta vacia
        boolean vacio = this.arista.isEmpty(); 
        // si la lista esta vacia retorna falso
        if (vacio == true)                          
            return false;
        else
            return true;
    }

    public void listarAdyacentes() {
        Iterator iter = this.arista.iterator();
        while (iter.hasNext()){
            // imprime todos los elementos de la lista ordenadamente
            System.out.println(iter.next());          
        }
    }

    public void renombrar(String nom) {
        this.nombre = nom;
    }

    public void agregarAdyacente(Nodo origen, Nodo destino, int peso) {
        Arco arquin = new Arco(origen, destino, peso);
        // agrega un arco a la lista
        arista.add(arquin);                       
    }
    
    public void modificarAdyacente(Nodo destino, int arco) {
        this.arista.get(arco).setDestino(destino);
    }

    public void eliminarAdyacente(int posicion) {
        // recibe la posicion del arco y la elimina
        this.arista.remove(posicion);                 
    }
    
}
