package grafos;

//Code by: Rodrigo Alcayaga, Francisco Thenoux
public class Arco {
    Nodo origen;
    Nodo destino;
    int peso;
    
    
    
    //:::::::: CONSTRUCTORES :::::::::::
    
    public Arco(Nodo inicio, Nodo fin, int valor) {
        
        origen = inicio;
        destino = fin;
        peso = valor;
                        
    }
    
     
    
    //::::::::: ACCESORES ::::::::::::
    
    void setOrigen(Nodo or){
        this.origen = or;
    }
    
    void setDestino(Nodo des){
        this.destino = des;
    }
    
    
    //:::::::: FUNCIONES Y METODOS :::::::::::
    
    public void actualizarPeso(int valor) {
        this.peso = valor;
    }
    
}
